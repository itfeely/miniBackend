package com.mini.minibackend.consant;

/**
 * 自定义 常量类
 */
public final class ConsantType {
    /**
     * 订单状态
     */
    static class OrderStatus{
        public static Integer 未支付 = 0;
    }

    /**
     * 菜单状态
     */
    static class MenuStatus{
        public static Integer 禁用 = 0;
        public static Integer 启用 = 1;
    }
}
