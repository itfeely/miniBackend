package com.mini.minibackend.service;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.mini.minibackend.utils.ResultUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;

@Service
public class GeneratorService {
    final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    JdbcTemplate jdbcTemplate;

    Snowflake snowflake = IdUtil.getSnowflake(1, 1);

    /**
     * 自定义Id初始策略
     * @param type
     * @param prefix 前缀唯一
     * @param initIndex 初始值建议以1开头，例如 10，100，1000。。。
     * @param desc
     * @return
     */
    @Transactional
    public String IdGenerator(String type,String prefix,Integer initIndex,String desc){
        String id = snowflake.nextIdStr();
        Integer curIndex = initIndex + 1;
        String sql = "insert into sysCustId(id,type,prefix,initIndex,curIndex,`desc`,useFlag,createDate) " +
                "values (?,?,?,?,?,?,?,?)";
        Integer i = jdbcTemplate.update(sql,new Object[]{id,type,prefix,initIndex,curIndex,desc,1, LocalDateTime.now()});
        if(i>0) {
            return curIndex.toString();
        }
        return ResultUtils.message("0001","ID记录生成失败",null);
    }

    @Transactional
    public String getId(String prefix) {
        String sql_query = "select curIndex from sysCustId where prefix = ?";
        Integer value = null;
        try {
            value = jdbcTemplate.queryForObject(sql_query,new Object[]{prefix},Integer.class);
        }catch (EmptyResultDataAccessException e){}
        if(null == value) return null;
        Integer curIndex = value + 1;
        String sql_update = "update sysCustId set curIndex=?,modifyDate=? where prefix = ?";
        jdbcTemplate.update(sql_update,new Object[]{curIndex,LocalDateTime.now(),prefix});
        return curIndex.toString();
    }
}
