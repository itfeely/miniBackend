package com.mini.minibackend.service;

import cn.hutool.Hutool;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.PageUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.mini.minibackend.utils.MiniToolUtils;
import com.mini.minibackend.utils.ResultUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.validation.constraints.Null;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SystemService {
    final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    JdbcTemplate jdbcTemplate;
    @Resource
    GeneratorService gService;

    Snowflake snowflake = IdUtil.getSnowflake(1, 1);
    /****************************************************用户管理*********************************************************/
    /**
     * 用户列表
     * @param page
     * @param limit
     * @return
     */
    public String getUserList(Integer page, Integer limit){
        int[] rowIndex = PageUtil.transToStartEnd(page-1,limit);//分页行数
        String sqlCount = "select count(*) from sysUser";
        String sql = "select su.*,(select GROUP_CONCAT(sr.`name`)  from sysRole sr where su.roleIds like CONCAT('%',sr.id,'%')) roleNames from sysUser su limit "+rowIndex[0]+","+rowIndex[1];
        String count = jdbcTemplate.queryForObject(sqlCount,String.class);
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
        return ResultUtils.message2Page("0000","查询成功",count,list);
    }

    /**
     * 用户详情
     * @param id
     * @return
     */
    public String showUser(String id){
        if(StrUtil.isEmpty(id)) return ResultUtils.message("0001","参数错误",null);
        logger.info(id);
        String sql = "select * from sysUser where id = ?";
        Map<String,Object> entity = jdbcTemplate.queryForMap(sql,id);
        return ResultUtils.message("0000","查询成功",entity);
    }

    /**
     * 添加/编辑用户
     * @param map
     * @return
     */
    @Transactional
    public String addOrEditUser(Map<String,Object> map){
        //事务回滚标记
        boolean rollbackFlag = false;
        //参数处理
        String id = null==map.get("id")?null:map.get("id").toString();
        String username = null==map.get("username")?null:map.get("username").toString();
        String password = null==map.get("password")?null:map.get("password").toString();
        String salt = null==map.get("salt")?null:map.get("salt").toString();
        String roleIds = null==map.get("roleIds")?null:map.get("roleIds").toString();
        String locked = null==map.get("locked")?null:map.get("locked").toString();
        String useFlag = null==map.get("useFlag")?null:map.get("useFlag").toString();
        LocalDateTime modifyDate = LocalDateTime.now(),createDate = LocalDateTime.now();
        //业务逻辑
        if(StrUtil.isEmpty(id)){//添加
            id = snowflake.nextIdStr();
            password = MiniToolUtils.PasswordUtils.encryptPassword(password,salt);
            String sql = "insert into sysUser(id,username,password,salt,roleIds,locked,useFlag,createDate) " +
                    "values (?,?,?,?,?,?,?,?)";
            Integer i = jdbcTemplate.update(sql,new Object[]{id,username,password,salt,roleIds,locked,useFlag,createDate});
            if(i>0) {
                return ResultUtils.message("0000","添加成功",jdbcTemplate.queryForMap("select *from sysUser where id = ?",id));
            }
        }else{//编辑
            String sql = "update sysUser set username=?,password=?,salt=?,roleIds=?,locked=?,useFlag=?,modifyDate=? where id = ? ";
            Integer i = jdbcTemplate.update(sql,new Object[]{username,password,salt,roleIds,locked,useFlag,modifyDate,id});
            if(i>0) {
                return ResultUtils.message("0000","更新成功",jdbcTemplate.queryForMap("select *from sysUser where id = ?",id));
            }
        }
        rollbackFlag = true;//业务失败 需要回滚
        if(rollbackFlag) TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//业务失败强制回滚
        return ResultUtils.message("0001","操作失败",null);
    }

    /**
     * 删除/批量删除用户
     * @param ids
     * @return
     */
    public String delUser(String ids){
        if(StrUtil.isEmpty(ids)) return ResultUtils.message("0001","参数错误",null);
        String sql = "delete from sysUser where id in ("+ids+")";
        Integer i = jdbcTemplate.update(sql);
        if(i>0) return ResultUtils.message("0000","删除成功",null);
        return ResultUtils.message("0001","删除失败",null);
    }

    @Transactional
    public String resetPassword(String id){
        Map<String,Object> map = jdbcTemplate.queryForMap("select salt from sysUser where id = ?",id);
        String passwrod = MiniToolUtils.PasswordUtils.encryptPassword("123456",map.get("salt").toString());//可动态设置密码
        Integer i = jdbcTemplate.update("update sysUser set password = ? where id = ?",passwrod,id);
        if(i>0) return ResultUtils.message("0000","重置成功",passwrod);
        return ResultUtils.message("0001","重置失败",null);
    }
    /****************************************************Code Over*********************************************************/
    /****************************************************角色管理*********************************************************/
    /**
     * 角色列表
     * @param page
     * @param limit
     * @return
     */
    public String getRoleList(Integer page, Integer limit){
        int[] rowIndex = PageUtil.transToStartEnd(page-1,limit);//分页行数
        String sqlCount = "select count(*) from sysRole";
        String sql = "select * from sysRole limit "+rowIndex[0]+","+rowIndex[1];
        String count = jdbcTemplate.queryForObject(sqlCount,String.class);
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
        return ResultUtils.message2Page("0000","查询成功",count,list);
    }

    /**
     * 角色详情
     * @param id
     * @return
     */
    public String showRole(String id){
        if(StrUtil.isEmpty(id)) return ResultUtils.message("0001","参数错误",null);
        logger.info(id);
        String sql = "select * from sysRole where id = ?";
        Map<String,Object> entity = jdbcTemplate.queryForMap(sql,id);
        return ResultUtils.message("0000","查询成功",entity);
    }

    /**
     * 添加/编辑角色
     * @param map
     * @return
     */
    public String addOrEditRole(Map<String, Object> map){
        //事务回滚标记
        boolean rollbackFlag = false;
        //参数处理
        String id = null==map.get("id")?null:map.get("id").toString();
        String name = null==map.get("name")?null:map.get("name").toString();
        String remark = null==map.get("remark")?null:map.get("remark").toString();
        String useFlag = "1";//默认生效
        LocalDateTime modifyDate = LocalDateTime.now(),createDate = LocalDateTime.now();
        //业务逻辑
        if(StrUtil.isEmpty(id)){//添加
            id = snowflake.nextIdStr();
            String sql = "insert into sysRole(id,name,remark,useFlag,createDate) " +
                    "values (?,?,?,?,?)";
            Integer i = jdbcTemplate.update(sql,new Object[]{id,name,remark,useFlag,createDate});
            if(i>0) {

                return ResultUtils.message("0000","添加成功",jdbcTemplate.queryForMap("select *from sysRole where id = ?",id));
            }
        }else{//编辑
            String sql = "update sysRole set name=?,remark=?,useFlag=?,modifyDate=? where id = ? ";
            Integer i = jdbcTemplate.update(sql,new Object[]{name,remark,useFlag,modifyDate,id});
            if(i>0) {
                return ResultUtils.message("0000","更新成功",jdbcTemplate.queryForMap("select *from sysRole where id = ?",id));
            }
        }
        rollbackFlag = true;//业务失败 需要回滚
        if(rollbackFlag) TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//业务失败强制回滚
        return ResultUtils.message("0001","操作失败", null);
    }

    /**
     * 删除/批量删除角色
     * @param ids
     * @return
     */
    public String delRole(String ids){
        if(StrUtil.isEmpty(ids)) return ResultUtils.message("0001","参数错误",null);
        String sql = "delete from sysRole where id in ("+ids+")";
        Integer i = jdbcTemplate.update(sql);
        if(i>0) return ResultUtils.message("0000","删除成功",null);
        return ResultUtils.message("0001","删除失败",null);
    }

    /**
     * 赋权限
     * @param id
     * @param ids
     * @return
     */
    @Transactional
    public String createRolePermsRel(String id,String ids){//事务回滚标记
        List<String> oldIds = jdbcTemplate.queryForList("select resourceId from sysRoleResourceRel where roleId = ?",String.class,id);
        List<String> newIds = new ArrayList<>(Arrays.asList(ids.split(",")));
        List<String> jiaoji =  new ArrayList<>(Arrays.asList(new String[oldIds.size()]));//赋值的目标表需要这样创建
        Collections.copy(jiaoji,oldIds);
        jiaoji.retainAll(newIds);//交集
        oldIds.removeAll(jiaoji);//差集 需要删除的ID
        newIds.removeAll(jiaoji);//差集 需要新增的ID
        List<Object[]> handleList = new ArrayList<>();
        for (String oldId:oldIds) {
            handleList.add(new Object[]{id,oldId});
        }
        int[] i = jdbcTemplate.batchUpdate("delete from sysRoleResourceRel where roleId=? and resourceId = ?",handleList);
        handleList.clear();
        for (String newId:newIds) {
            handleList.add(new Object[]{id,newId});
        }
        int[] j = jdbcTemplate.batchUpdate("insert into sysRoleResourceRel(roleId,resourceId) values (?,?)",handleList);
        return ResultUtils.message("0000","赋权成功",null);
    }
    public String getRolePermsRel(String id){
        List<String> oldIds = jdbcTemplate.queryForList("select resourceId from sysRoleResourceRel where roleId = ?",String.class,id);
        return ResultUtils.message("0000","赋权成功",oldIds);
    }
    /****************************************************Code Over*********************************************************/
    /****************************************************组织管理*********************************************************/
    /**
     * 组织列表
     * @param page
     * @param limit
     * @return
     */
    public String getOrgList(Integer page,Integer limit){
        int[] rowIndex = PageUtil.transToStartEnd(page-1,limit);//分页行数
        String sqlCount = "select count(*) from sysOrganization";
        String sql = "select * from sysOrganization limit "+rowIndex[0]+","+rowIndex[1];
        String count = jdbcTemplate.queryForObject(sqlCount,String.class);
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
        return ResultUtils.message2Page("0000","查询成功",count,list);
    }

    /**
     * 组织详情
     * @param id
     * @return
     */
    public String showOrg(String id){
        if(StrUtil.isEmpty(id)) return ResultUtils.message("0001","参数错误",null);
        logger.info(id);
        String sql = "select * from sysOrganization where id = ?";
        Map<String,Object> entity = jdbcTemplate.queryForMap(sql,id);
        return ResultUtils.message("0000","查询成功",entity);
    }

    /**
     * 添加/编辑组织
     * @param map
     * @return
     */
    @Transactional
    public String addOrEditOrg(Map<String, Object> map){
        //事务回滚标记
        boolean rollbackFlag = false;
        //参数处理
        String id = null==map.get("id")?null:map.get("id").toString();
        String name = null==map.get("name")?null:map.get("name").toString();
        String sort = null==map.get("sort")?null:map.get("sort").toString();
        String parentId = null==map.get("parentId")?null:map.get("parentId").toString();
        String parentIds = null==map.get("parentIds")?null:map.get("parentIds").toString();
        String useFlag = "1";//默认生效
        LocalDateTime modifyDate = LocalDateTime.now(),createDate = LocalDateTime.now();
        //业务逻辑
        if(StrUtil.isEmpty(id)){//添加
//            id = snowflake.nextIdStr();
            String prefix = "Org"+parentId;
            id = gService.getId(prefix);
            if(null==id) id = gService.IdGenerator("Org",prefix,10,"组织ID");
            String sql = "insert into sysOrganization(id,name,sort,parentId,parentIds,useFlag,createDate) " +
                    "values (?,?,?,?,?,?,?)";
            Integer i = jdbcTemplate.update(sql,new Object[]{parentId+id,name,sort,parentId,parentIds,useFlag,createDate});
            if(i>0) {

                return ResultUtils.message("0000","添加成功",jdbcTemplate.queryForMap("select *from sysOrganization where id = ?",parentId+id));
            }
        }else{//编辑
            String sql = "update sysOrganization set name=?,sort=?,parentId=?,parentIds=?,useFlag=?,modifyDate=? where id = ? ";
            Integer i = jdbcTemplate.update(sql,new Object[]{name,sort,parentId,parentIds,useFlag,modifyDate,id});
            if(i>0) {
                return ResultUtils.message("0000","更新成功",jdbcTemplate.queryForMap("select *from sysOrganization where id = ?",id));
            }
        }
        rollbackFlag = true;//业务失败 需要回滚
        if(rollbackFlag) TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//业务失败强制回滚
        return ResultUtils.message("0001","操作失败", null);
    }

    /**
     * 获取组织树结构 下拉组件使用
     * @return
     */
    public String getOrgTree(){
        //查询所有数据
        List<Map<String,Object>> slist = jdbcTemplate.queryForList("select * from sysOrganization where useFlag = 1 order by sort");
        //一级菜单
        List<Map<String,Object>> list1 = slist.stream().filter(x-> x.get("parentId").equals("")).collect(Collectors.toList());
//        list1.stream().forEach(x->{
//            //二级菜单
//            List<Map<String,Object>> list2 = slist.stream().filter(xx-> xx.get("parentId").equals(x.get("id"))).collect(Collectors.toList());
//            x.put("children",list2);
//            list2.stream().forEach(xx->{
//                //三级菜单
//                List<Map<String,Object>> list3 = slist.stream().filter(xxx-> xxx.get("parentId").equals(xx.get("id"))).collect(Collectors.toList());
//                xx.put("children",list3);
//                //四级、五级、六级...
//            });
//        });
        this.OrgDiGui(list1,slist);
        return JSONUtil.toJsonStr(list1);
    }

    /**
     * 无限极递归
     * @param list
     * @param slist
     */
    public void OrgDiGui(List<Map<String,Object>> list,List<Map<String,Object>> slist){
        list.stream().forEach(x->{
            List<Map<String,Object>> list2 = slist.stream().filter(xx-> xx.get("parentId").equals(x.get("id"))).collect(Collectors.toList());
            x.put("children",list2);
            this.OrgDiGui(list2,slist);
        });
    }
    /**
     * 删除/批量删除组织
     * @param ids
     * @return
     */
    public String delOrg(String ids){
        if(StrUtil.isEmpty(ids)) return ResultUtils.message("0001","参数错误",null);
        String sql = "delete from sysOrganization where id in ("+ids+")";
        Integer i = jdbcTemplate.update(sql);
        if(i>0) return ResultUtils.message("0000","删除成功",null);
        return ResultUtils.message("0001","删除失败",null);
    }
    /****************************************************Code Over*********************************************************/
    /****************************************************菜单管理*********************************************************/
    /**
     * 资源列表
     * @return
     */
    public String getResList(){
        String sql = "select * from sysResource ";
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
        return ResultUtils.message("0000","查询成功",list);
    }

    /**
     * 资源详情
     * @param id
     * @return
     */
    public String showRes(String id){
        if(StrUtil.isEmpty(id)) return ResultUtils.message("0001","参数错误",null);
        logger.info(id);
        String sql = "select * from sysResource where id = ?";
        Map<String,Object> entity = jdbcTemplate.queryForMap(sql,id);
        return ResultUtils.message("0000","查询成功",entity);
    }

    /**
     * 添加/编辑资源
     * @param map
     * @return
     */
    @Transactional
    public String addOrEditRes(Map<String,Object> map){
        //事务回滚标记
        boolean rollbackFlag = false;
        //参数处理
        String id = null==map.get("id")?null:map.get("id").toString();
        String name = null==map.get("name")?null:map.get("name").toString();
        String type = null==map.get("type")?null:map.get("type").toString();
        String pathUrl = null==map.get("pathUrl")?null:map.get("pathUrl").toString();
        String iconUrl = null==map.get("iconUrl")?null:map.get("iconUrl").toString();
        String sort = null==map.get("sort")?null:map.get("sort").toString();
        String parentId = null==map.get("parentId")?null:map.get("parentId").toString();
        String parentIds = null==map.get("parentIds")?null:map.get("parentIds").toString();
        String permission = null==map.get("permission")?null:map.get("permission").toString();
        String useFlag = "1";//默认生效?
        LocalDateTime modifyDate = LocalDateTime.now(),createDate = LocalDateTime.now();
        //业务逻辑
        if(StrUtil.isEmpty(id)){//添加
//            id = snowflake.nextIdStr();
            String prefix = "M"+parentId;
            id = gService.getId(prefix);
            if(null==id) id = gService.IdGenerator("Menu",prefix,10,"菜单ID");
            String sql = "insert into sysResource(id,name,type,pathUrl,iconUrl,sort,parentId,parentIds,permission,useFlag,createDate) " +
                    "values (?,?,?,?,?,?,?,?,?,?,?)";
            Integer i = jdbcTemplate.update(sql,new Object[]{parentId+id,name,type,pathUrl,iconUrl,sort,parentId,parentIds,permission,useFlag,createDate});
            if(i>0) {

                return ResultUtils.message("0000","添加成功",jdbcTemplate.queryForMap("select *from sysResource where id = ?",parentId+id));
            }
        }else{//编辑
            String sql = "update sysResource set name=?,type=?,pathUrl=?,iconUrl=?,sort=?,parentId=?,parentIds=?,permission=?,useFlag=?,modifyDate=? where id = ? ";
            Integer i = jdbcTemplate.update(sql,new Object[]{name,type,pathUrl,iconUrl,sort,parentId,parentIds,permission,useFlag,modifyDate,id});
            if(i>0) {
                return ResultUtils.message("0000","更新成功",jdbcTemplate.queryForMap("select *from sysResource where id = ?",id));
            }
        }
        rollbackFlag = true;//业务失败 需要回滚
        if(rollbackFlag) TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//业务失败强制回滚
        return ResultUtils.message("0001","操作失败", null);
    }

    /**
     * 删除/批量删除资源
     * @param ids
     * @return
     */
    public String delRes(String ids){
        if(StrUtil.isEmpty(ids)) return ResultUtils.message("0001","参数错误",null);
        String sql = "delete from sysResource where id in ("+ids+")";
        Integer i = jdbcTemplate.update(sql);
        if(i>0) return ResultUtils.message("0000","删除成功",null);
        return ResultUtils.message("0001","删除失败",null);
    }

    /**
     * 获取菜单树结构 下拉组件使用
     * @return
     */
    public String getResTree(){
        //查询所有数据
        List<Map<String,Object>> slist = jdbcTemplate.queryForList("select * from sysResource where useFlag = 1 and type !=2 order by sort");
        //一级菜单
        List<Map<String,Object>> list1 = slist.stream().filter(x-> x.get("parentId").equals("")).collect(Collectors.toList());
        list1.stream().forEach(x->{
            //二级菜单
            List<Map<String,Object>> list2 = slist.stream().filter(xx-> xx.get("parentId").equals(x.get("id"))).collect(Collectors.toList());
            x.put("children",list2);
            list2.stream().forEach(xx->{
                //三级菜单
                List<Map<String,Object>> list3 = slist.stream().filter(xxx-> xxx.get("parentId").equals(xx.get("id"))).collect(Collectors.toList());
                xx.put("children",list3);
                //四级、五级、六级...
            });
        });
        return JSONUtil.toJsonStr(list1);
    }
    /****************************************************Code Over*********************************************************/
    /****************************************************ID策略管理*********************************************************/
    public String addCustId(Map<String, Object> map) {
        //事务回滚标记
        boolean rollbackFlag = false;
        //参数处理
        String name = null==map.get("name")?null:map.get("name").toString();
        String type = null==map.get("type")?null:map.get("type").toString();
        String iconUrl = null==map.get("iconUrl")?null:map.get("iconUrl").toString();
        String sort = null==map.get("sort")?null:map.get("sort").toString();
        String parentId = null==map.get("parentId")?null:map.get("parentId").toString();
        String parentIds = null==map.get("parentIds")?null:map.get("parentIds").toString();
        String permission = null==map.get("permission")?null:map.get("permission").toString();
        String useFlag = "1";//默认生效
        LocalDateTime modifyDate = LocalDateTime.now(),createDate = LocalDateTime.now();
        String id = snowflake.nextIdStr();
        String sql = "insert into sysCustId(id,name,type,iconUrl,sort,parentId,parentIds,permission,useFlag,createDate) " +
                "values (?,?,?,?,?,?,?,?,?,?)";
        Integer i = jdbcTemplate.update(sql,new Object[]{id,name,type,iconUrl,sort,parentId,parentIds,permission,useFlag,createDate});
        if(i>0) {

            return ResultUtils.message("0000","添加成功",jdbcTemplate.queryForMap("select *from sysCustId where id = ?",id));
        }
        rollbackFlag = true;//业务失败 需要回滚
        if(rollbackFlag) TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//业务失败强制回滚
        return ResultUtils.message("0001","操作失败", null);
    }

    public String getCustIdList(Integer page, Integer limit) {
        int[] rowIndex = PageUtil.transToStartEnd(page-1,limit);//分页行数
        String sqlCount = "select count(*) from sysCustId";
        String sql = "select * from sysCustId limit "+rowIndex[0]+","+rowIndex[1];
        String count = jdbcTemplate.queryForObject(sqlCount,String.class);
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql);
        return ResultUtils.message2Page("0000","查询成功",count,list);
    }
    /****************************************************Code Over*********************************************************/


}
