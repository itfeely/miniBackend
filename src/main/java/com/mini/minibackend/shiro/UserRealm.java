package com.mini.minibackend.shiro;

import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;
import java.util.*;

public class UserRealm extends AuthorizingRealm {
    @Resource
    JdbcTemplate jdbcTemplate;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = (String)principals.getPrimaryPrincipal();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        String roleIds = jdbcTemplate.queryForObject("select roleIds from sysUser where username = ?",String.class,username);
        authorizationInfo.setRoles(new HashSet<>(Arrays.asList(roleIds.split(","))));
        List<String> perms = jdbcTemplate.queryForList("SELECT a.permission FROM sysResource a WHERE EXISTS (SELECT b.resourceId FROM sysRoleResourceRel b WHERE a.id=b.resourceId AND b.roleId IN ("+roleIds+")) and a.permission !='' ",String.class);
        authorizationInfo.setStringPermissions(new HashSet<>(perms));
        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String username = (String)token.getPrincipal();
        Map<String,Object> user = new HashMap<String,Object>();
        try {
            user = jdbcTemplate.queryForMap("select username,password,salt,locked from sysUser where username = ?",username);
        }catch (Exception e){
            throw new UnknownAccountException();//没找到帐号
        }
        if("0".equals(user.get("locked").toString())) {
            throw new LockedAccountException(); //帐号锁定
        }
        //交给AuthenticatingRealm使用CredentialsMatcher进行密码匹配，如果觉得人家的不好可以在此判断或自定义实现
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                user.get("username").toString(), //用户名
                user.get("password").toString(), //密码
                ByteSource.Util.bytes(user.get("salt").toString()),//salt=username+salt
                getName()  //realm name
        );
        return authenticationInfo;
    }
}
