package com.mini.minibackend.shiro;

import com.mini.minibackend.utils.MiniToolUtils;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

public class MyCredentialsMatcher extends SimpleCredentialsMatcher {
    @Resource
    JdbcTemplate jdbcTemplate;
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        UsernamePasswordToken inToken=(UsernamePasswordToken) token;
        //获得用户输入的密码:(可以采用加盐(salt)的方式去检验)
        String inPassword = new String(inToken.getPassword());
        String inUsername = inToken.getUsername();
        Map<String,Object> user = jdbcTemplate.queryForMap("select username,password,salt,locked from sysUser where username = ?",inUsername);
        String salt = user.get("salt").toString();//数据库获取
        inPassword = MiniToolUtils.PasswordUtils.encryptPassword(inPassword,salt);
        String dbPassword = getCredentials(info).toString();
        return inPassword.equals(dbPassword);
    }
}
