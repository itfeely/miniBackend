package com.mini.minibackend.thymeleaf;

import com.mini.minibackend.service.SystemService;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.spring5.context.SpringContextUtils;
import org.thymeleaf.templatemode.TemplateMode;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

public class RoleProcessor extends AbstractAttributeTagProcessor {

    protected RoleProcessor(TemplateMode templateMode, String dialectPrefix, String elementName, boolean prefixElementName, String attributeName, boolean prefixAttributeName, int precedence, boolean removeAttribute) {
        super(templateMode, dialectPrefix, elementName, prefixElementName, attributeName, prefixAttributeName, precedence, removeAttribute);
    }
    @Override
    protected void doProcess(ITemplateContext context, IProcessableElementTag tag, AttributeName attributeName, String attributeValue, IElementTagStructureHandler structureHandler) {
        final ApplicationContext appCtx = SpringContextUtils.getApplicationContext(context);
        JdbcTemplate jdbcTemplate = appCtx.getBean(JdbcTemplate.class);
        List<Map<String,Object>> list = jdbcTemplate.queryForList("select * from sysRole");
        String tagText = "";
        for (Map<String, Object> x : list) {
            tagText = tagText + ("<input type='checkbox' lay-filter='" + attributeValue + "' title='" + x.get("name").toString() + "' value='" + x.get("id").toString() + "' lay-skin='primary' />");
        }
        //        String text = "<input type='checkbox' name='roleIds' title='管理员' lay-skin='primary' />";
        structureHandler.setBody(tagText,false);
    }
}
