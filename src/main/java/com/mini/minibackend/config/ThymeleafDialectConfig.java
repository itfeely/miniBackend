package com.mini.minibackend.config;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import com.mini.minibackend.thymeleaf.MyTagDialect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.standard.StandardDialect;

@Configuration
public class ThymeleafDialectConfig {
    @Bean
    public MyTagDialect myTagDialect(){
        return new MyTagDialect("sys dialect","mytag", StandardDialect.PROCESSOR_PRECEDENCE);
    }

    @Bean
    public ShiroDialect shiroDialect() {
        return new ShiroDialect();
    }
}
