package com.mini.minibackend.config;

import com.mini.minibackend.shiro.MyCredentialsMatcher;
import com.mini.minibackend.shiro.UserRealm;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.LinkedHashMap;
import java.util.Map;

@EnableAsync
@EnableCaching
@Configuration
public class ShiroConfig {
    @Bean
    UserRealm userRealm() {
        return new UserRealm();
    }

    @Bean
    DefaultWebSecurityManager securityManager() {
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
        userRealm().setCredentialsMatcher(credentialsMatcher());
        manager.setRealm(userRealm());
        return manager;
    }

    @Bean
    ShiroFilterFactoryBean shiroFilterFactoryBean() {
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        bean.setSecurityManager(securityManager());
//        bean.setLoginUrl("/");//认证失败则跳该接口
//        bean.setSuccessUrl("/login");
//        bean.setUnauthorizedUrl("/unauthorizedurl");
        Map<String, String> map = new LinkedHashMap<>();
        map.put("/static/**", "anon");
        bean.setFilterChainDefinitionMap(map);
        return bean;
    }

    @Bean
    CredentialsMatcher credentialsMatcher(){
        return new MyCredentialsMatcher();
    }
}
