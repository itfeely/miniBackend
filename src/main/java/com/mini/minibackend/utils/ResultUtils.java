package com.mini.minibackend.utils;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

public class ResultUtils {

    public static String message(String code,String msg,Object data){
        JSONObject jsonObject = JSONUtil.createObj();
        jsonObject.putOnce("code",code);
        jsonObject.putOnce("msg",msg);
        jsonObject.putOnce("data",data);
        return JSONUtil.toJsonStr(jsonObject);
    }

    public static String message2Page(String code,String msg,String count,Object data){
        JSONObject jsonObject = JSONUtil.createObj();
        jsonObject.putOnce("code",code);
        jsonObject.putOnce("msg",msg);
        jsonObject.putOnce("count",count);
        jsonObject.putOnce("data",data);
        return JSONUtil.toJsonStr(jsonObject);
    }
}
