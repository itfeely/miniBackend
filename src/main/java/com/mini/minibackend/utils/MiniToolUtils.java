package com.mini.minibackend.utils;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

public class MiniToolUtils {
    public static class StringUtils {
        //补全位数
        public static String  getSerialNumber(String str,int length) {
            StringBuffer sb = new StringBuffer();
            //补位数
            int n = length-str.length();
            for(int i = 0;i<n;i++) {
                sb.append("0");
            }
            sb.append(str);
            return sb.toString();
        }
    }
    public static class PasswordUtils{
        //密码加密
        public static String encryptPassword(String password,String salt){
            return new SimpleHash("MD5",password, ByteSource.Util.bytes(salt),2).toHex();//新增 密码加密
        }
    }
}
