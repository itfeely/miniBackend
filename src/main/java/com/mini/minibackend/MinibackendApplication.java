package com.mini.minibackend;

import org.apache.shiro.spring.boot.autoconfigure.ShiroAnnotationProcessorAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(exclude = {ShiroAnnotationProcessorAutoConfiguration.class})
public class MinibackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinibackendApplication.class, args);
    }

}
