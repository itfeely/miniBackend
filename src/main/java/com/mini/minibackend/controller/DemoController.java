package com.mini.minibackend.controller;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.mini.minibackend.utils.ResultUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
public class DemoController {
    @Resource
    JdbcTemplate jdbcTemplate;
    @Resource
    RestTemplate restTemplate;
    @Resource
    RedisTemplate redisTemplate;

    @GetMapping("admin/system/getUserList")
    public String getUserlist(){
        String sql = "select * from user_main where userId = ? and userNickName like CONCAT('%',?,'%')";
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql,"U1000132","Apple");

        return ResultUtils.message("0000","查询成功",list);
    }

    @GetMapping("admin/system/getThirdApi")
    public String getThirdApi(){
        String url = "http://www.msece.com/waether/upload/weather/json/NationalUrbanData.min.json";
//        HttpEntity<MultiValueMap<String,Object>> httpEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>();
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url,String.class,"");
        return responseEntity.getBody();
    }

    @GetMapping("admin/system/getRedisData")
    public String getRedisData(){
        redisTemplate.opsForValue().setIfAbsent("system","Hello Redis",20000, TimeUnit.MILLISECONDS);
        String result = redisTemplate.opsForValue().get("system").toString();
        return result;
    }
}
