package com.mini.minibackend.controller;

import com.mini.minibackend.service.GeneratorService;
import com.mini.minibackend.service.SystemService;
import com.mini.minibackend.utils.ResultUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 系统管理
 */
@Controller
public class SystemController {
    final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    JdbcTemplate jdbcTemplate;
    @Resource
    SystemService service;
    /**
     * 样例页面跳转 按需修改
     * @return
     */
    @GetMapping("/demo/{pageName}")
    public String demo(@PathVariable(name="pageName") String pageName){
        return "demo/"+pageName;
    }

    /**
     * 所有后台页面跳转 按需拓展  注意！！！！！！！！！！！！
     * @return
     */
    @GetMapping("/admin/{pathName}/{pathName2}")
    public String admin(@PathVariable(name="pathName") String pathName, @PathVariable(name="pathName2") String pathName2, Model model){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String id = request.getParameter("id");
        String optType = request.getParameter("optType");
        model.addAttribute("id",id);
        model.addAttribute("optType",optType);
        return pathName+"/"+pathName2;
    }

    /**
     * 首页
     * @return
     */
    @GetMapping("/index")
    public String index(){
        return "index";
    }
    /**
     * 登录页面
     * @return
     */
    @GetMapping("/")
    public String loginPage(){
        return "login";
    }
    /**
     * 跳转主界面
     * @return
     */
    @GetMapping("/main")
    public String mainPage(String username,Model model) throws UnsupportedEncodingException{
        username = URLDecoder.decode(username,"utf-8");
        Map<String,Object> user = jdbcTemplate.queryForMap("select * from sysUser where username = ?",username);
        model.addAttribute("user",user);
        return "main";
    }
    /**
     * 跳转主界面
     * @return
     */
    @PostMapping("/login")
    @ResponseBody
    public String login(String username,String password) {
        UsernamePasswordToken token = new UsernamePasswordToken(username,password);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
        } catch (UnknownAccountException e){
            return ResultUtils.message("0001","账户不存在",null);
        } catch (LockedAccountException e) {
            return ResultUtils.message("0001","账户已锁定",null);
        } catch (IncorrectCredentialsException e){
            return ResultUtils.message("0001","账户密码错误",null);
        }
        return ResultUtils.message("0000","登录成功",username);
    }

    /**
     * 菜单列表(树形)
     * @return
     */
    @GetMapping("admin/menuAll")
    @ResponseBody
    public String menuAll(String userId){
        //获取角色
        String roleIds = jdbcTemplate.queryForObject("select roleIds from sysUser where id = ?",String.class,userId);
        //查询所有数据
        List<Map<String,Object>> slist = jdbcTemplate.queryForList("SELECT a.* FROM sysResource a WHERE EXISTS (SELECT b.resourceId FROM sysRoleResourceRel b WHERE a.id=b.resourceId AND b.roleId IN ("+roleIds+")) and a.useFlag = 1 and a.type !=2 order by a.sort");
        //一级菜单
        List<Map<String,Object>> list1 = slist.stream().filter(x-> x.get("parentId").equals("")).collect(Collectors.toList());
        list1.stream().forEach(x->{
            //二级菜单
            List<Map<String,Object>> list2 = slist.stream().filter(xx-> xx.get("parentId").equals(x.get("id"))).collect(Collectors.toList());
            x.put("children",list2);
            list2.stream().forEach(xx->{
                //三级菜单
                List<Map<String,Object>> list3 = slist.stream().filter(xxx-> xxx.get("parentId").equals(xx.get("id"))).collect(Collectors.toList());
                xx.put("children",list3);
                //四级、五级、六级...
            });
        });
        return ResultUtils.message("0000","查询成功",list1);
    }
    /****************************************************用户管理*********************************************************/
    @GetMapping("admin/user/getUserList")
    @ResponseBody
    public String getUserList(@RequestParam(name = "page",required = true,defaultValue = "1") Integer page,
                              @RequestParam(name = "limit",required = true,defaultValue = "10") Integer limit){
        return service.getUserList(page,limit);
    }
    @GetMapping("admin/user/showUser")
    @ResponseBody
    public String showUser(String id){
        return service.showUser(id);
    }
    @PostMapping("admin/user/addOrEditUser")
    @ResponseBody
    public String addOrEditUser(@RequestParam Map<String,Object> map){
        return service.addOrEditUser(map);
    }
    @GetMapping("admin/user/delUser")
    @ResponseBody
    public String delUser(String ids){
        return service.delUser(ids);
    }
    @GetMapping("admin/user/resetPassword")
    @ResponseBody
    public String resetPassword(String id){
        return service.resetPassword(id);
    }

    /****************************************************Code Over*********************************************************/
    /****************************************************角色管理*********************************************************/
    @GetMapping("admin/role/getRoleList")
    @ResponseBody
    public String getRoleList(@RequestParam(name = "page",required = true,defaultValue = "1") Integer page,
                             @RequestParam(name = "limit",required = true,defaultValue = "10") Integer limit){
        return service.getRoleList(page,limit);
    }
    @GetMapping("admin/role/showRole")
    @ResponseBody
    public String showRole(String id){
        return service.showRole(id);
    }
    @PostMapping("admin/role/addOrEditRole")
    @ResponseBody
    public String addOrEditRole(@RequestParam Map<String,Object> map){
        return service.addOrEditRole(map);
    }
    @GetMapping("admin/role/delRole")
    @ResponseBody
    public String delRole(String ids){
        return service.delRole(ids);
    }
    @GetMapping("admin/role/createRolePermsRel")
    @ResponseBody
    public String createRolePermsRel(String id,String ids){
        return service.createRolePermsRel(id,ids);
    }
    @GetMapping("admin/role/getRolePermsRel")
    @ResponseBody
    public String getRolePermsRel(String id){
        return service.getRolePermsRel(id);
    }
    /****************************************************Code Over*********************************************************/
    /****************************************************组织管理*********************************************************/
    @GetMapping("admin/org/getOrgList")
    @ResponseBody
    public String getOrgList(@RequestParam(name = "page",required = true,defaultValue = "1") Integer page,
                              @RequestParam(name = "limit",required = true,defaultValue = "10") Integer limit){
        return service.getOrgList(page,limit);
    }
    @GetMapping("admin/org/showOrg")
    @ResponseBody
    public String showOrg(String id){
        return service.showOrg(id);
    }
    @PostMapping("admin/org/addOrEditOrg")
    @ResponseBody
    public String addOrEditOrg(@RequestParam Map<String,Object> map){
        return service.addOrEditOrg(map);
    }
    @GetMapping("admin/org/getOrgTree")
    @ResponseBody
    public String getOrgTree(){
        return service.getOrgTree();
    }
    @GetMapping("admin/org/delOrg")
    @ResponseBody
    public String delOrg(String ids){
        return service.delOrg(ids);
    }
    /****************************************************Code Over*********************************************************/
    /****************************************************菜单管理*********************************************************/
    @GetMapping("admin/res/getResList")
    @ResponseBody
    public String getResList(){
        return service.getResList();
    }
    @GetMapping("admin/res/showRes")
    @ResponseBody
    public String showRes(String id){
        return service.showRes(id);
    }
    @PostMapping("admin/res/addOrEditRes")
    @ResponseBody
    public String addOrEditRes(@RequestParam Map<String,Object> map){
        return service.addOrEditRes(map);
    }
    @GetMapping("admin/res/delRes")
    @ResponseBody
    public String delRes(String ids){
        return service.delRes(ids);
    }

    @GetMapping("admin/res/getResTree")
    @ResponseBody
    public String getResTree(){
        return service.getResTree();
    }
    /****************************************************Code Over*********************************************************/

    /****************************************************自定义层级ID*********************************************************/
    public String getCustIdList(@RequestParam(name = "page",required = true,defaultValue = "1") Integer page,
                                @RequestParam(name = "limit",required = true,defaultValue = "10") Integer limit) {
        return service.getCustIdList(page,limit);
    }
    public String addCustId(@RequestParam Map<String,Object> map) {
        return service.addCustId(map);
    }

    /****************************************************Code Over*********************************************************/
}
