package com.mini.minibackend.filter;


import cn.hutool.http.Method;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Component
public class RequestFilter implements Filter {
    final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("初始化RequestFilter过滤器");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String queryString = httpRequest.getQueryString();
        Map<String,String[]> data = httpRequest.getParameterMap();
        String url = httpRequest.getRequestURL().toString()+ (queryString == null ? "" : ("?" + httpRequest.getQueryString()));
        if(url.indexOf("static")<0) logger.info(url);
        if(Method.POST.equals(httpRequest.getMethod())&&data.keySet().size()>0) {
            String msg = "{";
            for (String key:data.keySet()) {
                msg += key + "->" + data.get(key)[0]+"#";
            }
            msg += "}";
            logger.info(msg);
        }
        chain.doFilter(httpRequest,httpResponse);
    }

    @Override
    public void destroy() {
        logger.info("销毁RequestFilter过滤器");
    }
}
