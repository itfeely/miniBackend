//提交表单
function submitForm(filter,url,fn) {
    var form = layui.form;
    var $ = layui.jquery;
    form.on(filter, function(data){
        // console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
        $.ajax({
            type: "post",//类型
            url: url,
            data: data.field,//参数赋值，后台获取参数保持一致
            dataType: 'json',
            success: function (response) { //成功否， back， 服务器端响应度信息
                if(response.code!="0000") {
                    layer.alert("保存失败");
                }else{
                    layer.alert("保存成功");
                    fn(response);//自定义回调赋值函数
                }
            },
            error: function () {//如果错误则错误信息。
                layer.alert("接口请求错误");
            }
        });
        return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
    });
}
//初始化form
function initForm(url,fn) {
    var $ = layui.jquery;
    if(id){
        $.ajax({
            type: "get",//类型
            url: url,
            data: {id:id},//参数赋值，后台获取参数保持一致
            dataType: 'json',
            success: function (response) { //成功否， back， 服务器端响应度信息
                if(response.code!="0000") {
                    layer.alert("获取失败");
                    return;
                }
                fn(response);//自定义回调赋值函数
            },
            error: function () {//如果错误则错误信息。
                layer.alert("接口请求错误");
            }
        });
    }
}
//弹框控制
function openWindow(title,url,fn) {
    var $ = layui.jquery;
    var index = layui.layer.open({
        title : title,
        type : 2,
        content : url,
        success : function(layero, index){
            setTimeout(function(){
                layui.layer.tips('点击此处返回文章列表','.layui-layer-setwin .layui-layer-close', {
                    tips: 3
                });
            },500)
        },
        cancel:function (index, layero) {
            fn();//回调用来刷新主页面信息
        }
    })
    layui.layer.full(index);
    // $(window).on("resize",function(){//改变窗口大小时，重置弹窗的宽高，防止超出可视区域（如F12调出debug的操作）
    //     layui.layer.full(index);
    // })
}
